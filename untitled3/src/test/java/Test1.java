import org.junit.Test;
import static org.junit.Assert.*;

public class Test1 {
    @Test
    public void TestPacking(){
        Packing packing = new Packing("Пакет",0.1);
        Packing packing1 = new Packing("Пакет", 0.1);
        Packing packing2 = new Packing("Солофан",0.2);
        assertEquals(packing.getName(),"Пакет");
        assertEquals(packing.getMass(), 0.1, 1E-9);
        assertNotEquals(packing2.getMass(),0.1);
        assertEquals(packing,packing1);
        assertNotSame(packing,packing1);
        assertNotEquals(packing,packing2);
        try {
            Packing packingFail = new Packing("Пакет", 0);
            fail("NullPointerException");
        }catch (NullPointerException thrown){
            assertNotEquals(thrown.getMessage(),"");
        }
        try {
            Packing packingFail = new Packing("", 0.1);
        }catch (NullPointerException thrown){
            assertNotEquals(thrown.getMessage(),"");
        }
    }

    @Test
    public void TestProduct(){
        Product product = new Product("Ручка","Пишет");
        Product product1 = new Product("Ручка","Пишет");
        Product product2 = new Product("Pen","Write");
        assertEquals(product,product1);
        assertNotEquals(product,product2);
        Product pieceGoods = new PieceGoods("Ручка","Пишет",0.1);
        PieceGoods pieceGoods1 = new PieceGoods("Ручка","Пишет",0.1);
        assertEquals(pieceGoods,pieceGoods1);
        Product pieceGoods2 = pieceGoods;
        assertSame(pieceGoods2,pieceGoods);
        Product product3 = new WeightItem("Резинка","Стирает");
        WeightItem weightItem = new WeightItem("Резинка","Стирает");
        assertEquals(product3,weightItem);
        pieceGoods2 = weightItem;
        assertSame(pieceGoods2,weightItem);
    }

    @Test
    public void TestPackingProduct(){
        Packing packing = new Packing("Пакет",0.1);
        PieceGoods pieceGoods = new PieceGoods("Ручка","Синяя",0.2);
        PackagedPieceGoods packagedPieceGoods = new PackagedPieceGoods(packing, pieceGoods,3);
        WeightItem weightItem = new WeightItem("Резинка","Белая");
        PackagedWeightGoods packagedWeightGoods = new PackagedWeightGoods(weightItem,0.4,packing);
        Product[] products ={packagedPieceGoods,packagedWeightGoods};
        Product[] products1={packagedWeightGoods,packagedWeightGoods};
        Product[] products2 = {weightItem,pieceGoods,packagedPieceGoods,packagedWeightGoods};
        Shipment shipment  = new Shipment(products,"Список домой");
        Shipment shipment1 = new Shipment(products1,"Список домой");
        assertNotEquals(shipment,shipment1);
        try {
            Shipment shipment2 = new Shipment(products2,"Список домой");
            fail("Expected NullPoint");
        }catch (NullPointerException thrown) {
            assertNotEquals("", thrown.getMessage());
        }
    }

    @Test
    public void TestMassProduct(){
        Packing packing = new Packing("Пакет", 0.01);
        WeightItem weightItem1 = new WeightItem("Резинка", "Белая");
        PackagedWeightGoods packagedWeightGoods = new PackagedWeightGoods(weightItem1,1,packing);
        PieceGoods pieceGoods1 = new PieceGoods("Блокнот","Маленький",0.7);
        PackagedPieceGoods packagedPieceGoods = new PackagedPieceGoods(packing, pieceGoods1, 5);
        assertEquals(packagedPieceGoods.getMussNetto(),3.5, 1E-9);
        assertEquals(packagedPieceGoods.getMussBrutto(),3.51,1E-9);
        assertEquals(packagedWeightGoods.getMuss(),1, 1E-9);
        assertEquals(packagedWeightGoods.getBrutto(),1.01,1E-9);
        Product[] products1 ={packagedPieceGoods,packagedWeightGoods};
        Shipment shipment1 = new Shipment(products1,"Покупка");
        assertEquals(shipment1.allMuss(),4.52,1E-9);
    }

    @Test
    public void TestFilter(){
        String str = "Я купил ручку";
        BeginStringFilter filter1 = new BeginStringFilter("Я");
        BeginStringFilter filter2 = new BeginStringFilter("купил");
        boolean res1 = filter1.apply(str);
        boolean res2 = filter2.apply(str);
        assertTrue(res1);
        assertFalse(res2);
        CountPattern filter11 = new CountPattern("купил");
        CountPattern filter12 = new CountPattern("блокнот");
        SubstringFilter filter3 = new SubstringFilter("Я купил");
        SubstringFilter filter4 = new SubstringFilter("Я блокнот");
        assertTrue(filter11.apply(str));
        assertFalse(filter12.apply(str));
        assertTrue(filter3.apply(str));
        assertFalse(filter4.apply(str));
    }

    @Test
    public void TestProductService(){
        Packing packing = new Packing("Пакет",0.01);
        WeightItem weightItem1 = new WeightItem("Ручка","Зеленая");
        WeightItem weightItem2 = new WeightItem("Ручка","Красная");
        PieceGoods pieceGoods1 = new PieceGoods("Блокнот","Синий",0.2);
        PieceGoods pieceGoods2 = new PieceGoods("Блокнот","Чёрный",0.1);
        PackagedWeightGoods packagedWeightGoods1 = new PackagedWeightGoods(weightItem1,1,packing);
        PackagedWeightGoods packagedWeightGood2 = new PackagedWeightGoods(weightItem2,1,packing);
        PackagedPieceGoods packagedPieceGoods1 = new PackagedPieceGoods(packing,pieceGoods1,3);
        PackagedPieceGoods packagedPieceGoods2 = new PackagedPieceGoods(packing,pieceGoods2,4);
        Product[] products = {packagedPieceGoods1,packagedPieceGoods2,packagedWeightGood2,packagedWeightGoods1};
        Shipment shipment = new Shipment(products,"Покупка");
        Filter filter1 = new SubstringFilter("Блокнот");
        Filter filter2 = new SubstringFilter("Ручка");
        assertEquals(ProductService.countByFilter(shipment,filter1),2);
        assertEquals(ProductService.countByFilter(shipment,filter2),2);
    }
}
