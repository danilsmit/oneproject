public class BeginStringFilter implements Filter {
    private String pattern;

    public BeginStringFilter(String pattern){
        this.pattern = pattern;
    }

    public String getPattern() {
        return pattern;
    }

    public void setPattern(String pattern) {
        this.pattern = pattern;
    }

    @Override
    public boolean apply(String string) {
        return string.startsWith(pattern);
    }
}

