public class SubstringFilter implements Filter {
    private String pattern;
    public SubstringFilter(String pattern){
        if (pattern == null)
            throw new NullPointerException("Пустая строка");
        else
            this.pattern = pattern;
    }


    public boolean apply(String string) {
        return string.contains(pattern);
    }
}
