import java.util.Objects;

public class PackagedWeightGoods extends WeightItem{
    private Packing packing;
    private double muss;

    public PackagedWeightGoods(WeightItem product, double muss, Packing packing){
        super(product);
        if (packing == null)
            throw new NullPointerException("");
        this.packing = packing;
        if (muss <=0)
            throw new IndexOutOfBoundsException("");
        this.muss = muss;
    }


    public Packing getPacking() {
        return packing;
    }

    public void setPacking(Packing packing) {
        this.packing = packing;
    }

    public double getMuss() {
        return muss;
    }

    public void setWeightItemsMuss(double Muss) {
        this.muss = Muss;
    }

    public double getBrutto(){
        return muss + packing.getMass();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof PackagedWeightGoods)) return false;
        if (!super.equals(o)) return false;
        PackagedWeightGoods that = (PackagedWeightGoods) o;
        return getMuss() == that.getMuss() &&
                Objects.equals(getPacking(), that.getPacking());
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), getPacking(), getMuss());
    }

    @Override
    public String toString() {
        return "PackagedWeightGoods{" +
                "packing=" + packing +
                ", muss=" + muss +
                "} " + super.toString();
    }
}
