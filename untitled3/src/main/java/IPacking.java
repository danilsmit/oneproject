public interface IPacking {
    double getMass();
    String getName();
}
