
import java.util.Objects;

public class PackagedPieceGoods extends  PieceGoods{
    private Packing packing;
    private PieceGoods piece;
    private int n;

    public PackagedPieceGoods(Packing packing, PieceGoods piece, int n ){
        super(piece);
        this.packing = packing;
        if (n < 0)
            throw new ArrayIndexOutOfBoundsException("");
        else{
            this.piece = piece;
            this.n = n;
        }
    }

    public Packing getPacking() {
        return packing;
    }

    public void setPacking(Packing packing) {
        this.packing = packing;
    }

    public double getMussNetto(){
        return n * piece.getMuss();
    }

    public double getMussBrutto(){
        return getMussNetto() + packing.getMass();
    }

    @Override
    public String toString() {
        return "PackagedPieceGoods{" +
                "packing=" + packing +
                ", piece=" + piece +
                ", n=" + n +
                "} " + super.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        PackagedPieceGoods that = (PackagedPieceGoods) o;
        return n == that.n &&
                Objects.equals(packing, that.packing) &&
                Objects.equals(piece, that.piece);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), packing, piece, n);
    }

    public PieceGoods getPiece() {
        return piece;
    }

    public void setPiece(PieceGoods piece) {
        this.piece = piece;
    }

    public int getN() {
        return n;
    }

    public void setN(int n) {
        this.n = n;
    }
}

