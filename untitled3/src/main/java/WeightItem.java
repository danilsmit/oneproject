public class WeightItem extends Product{
    public WeightItem(String name, String description){
        super(name,description);
    }

    public WeightItem(WeightItem weightItem){
        this(weightItem.getName(),weightItem.getDescription());
    }

    @Override
    public String toString() {
        return "WeightItem{" +
                "name='" + super.getName() + '\'' +
                "," + " description='" + super.getDescription() + '\'' +
                "}";
    }
}
