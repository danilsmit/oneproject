import java.util.Objects;

public class PieceGoods extends Product{
    private double muss;

    public PieceGoods(String name, String description, double muss){
        super(name,description);
        if (muss <= 0 )
            throw new NullPointerException("");
        this.muss = muss;
    }

    public PieceGoods(PieceGoods pieceGoods){
        this(pieceGoods.getName(),pieceGoods.getDescription(),pieceGoods.muss);
    }

    public double getMuss() {
        return muss;
    }

    public void setMuss(double muss) {
        this.muss = muss;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof PieceGoods)) return false;
        if (!super.equals(o)) return false;
        PieceGoods that = (PieceGoods) o;
        return getMuss() == that.getMuss();
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), getMuss());
    }

    @Override
    public String toString() {
        return "PieceGoods{" +
                "muss=" + muss + "," +
                " name='" + super.getName() + '\'' +
                "," + " description='" + super.getDescription() + '\'' +
                "}";
    }
}
