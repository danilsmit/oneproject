***1.1. Класс «Упаковка товара». Упаковка имеет название и массу в кг (собственная масса
упаковки).***
Класс `Packing`.

***1.2. Класс «Товар». Товар имеет название и описание (строки).***
Класс `Product`.

***1.3. Класс «Весовой товар». Весовой товар хранит название и описание.***
Класс `WeightItem`.

***1.4. Класс «Штучный товар». Штучный товар хранит название, описание и вес одной
штуки товара (в кг). Должен быть конструктор по всем трем полям.***
Класс `PieceGoods` содержит конструктор по всем трём полям.

***1.5. Класс «Упакованный весовой товар» содержит упаковку и некоторое количество
весового товара (в кг). Методы: конструктор по товару, массе товара и упаковке,
получить массу нетто (только товара) и массу брутто (упаковки и товара вместе).***
Класс `PackagedWeightGoods`.

***1.6. Класс «Упакованный штучный товар» содержит упаковку и несколько единиц
штучного товара. Методы: получить количество штук товара, получить массу нетто
(суммарный вес всех единиц товара) и массу брутто (упаковки и всех единиц товара
вместе).***
Класс `PackagedPieceGoods` конструктор реализует получение единиц штучного товара.

***1.8. Класс «Партия товаров». Партия товаров имеет описание и содержит некоторое
количество упакованных товаров, возможно, разных типов (в том числе наборов
товаров). Методы: конструктор по произвольному набору упаковок товаров,
получить массу всей партии (суммарная масса брутто всех товаров в партии).***
Класс `Shipment`.
***2.1. Интерфейс «Фильтр» с единственным методом apply. Метод получает на вход строку
и возвращает логическое значение (строка удовлетворяет условию фильтра или нет).***
Интерфейс Filter.
Класс `SubstringFilter`.

***2.2. Класс BeginStringFilter, реализующий этот интерфейс, со следующим поведением.
При создании объекта класса конструктор получает на вход и сохраняет строку
pattern. Метод apply(str) проверяет, что строка str начинается с подстроки pattern.
Т.е. должен работать следующий фрагмент кода:
String str = “Мама мыла раму”;
BeginStringFilter filter1 = new BeginStringFilter(“Мама”);
BeginStringFilter filter2 = new BeginStringFilter(“мыла”);
boolean res1 = filter1.apply(str); // результат — true
boolean res2 = filter2.apply(str); // результат — false***

Класс `BeginStringFilter`. Метод startsWith проверяет начало строки.

***2.3. Напишите еще две какие-нибудь реализации интерфейса «Фильтр».***
Класс `BeginStringFilter`.

***3. Напишите класс «Сервис товаров» со следующими методами.
3.1. Метод countByFilter, который получает на вход партию товара и произвольный
фильтр из п.2. Метод возвращает количество элементов партии, имена которых
удовлетворяют фильтру. Для товаров-наборов анализируется только название самого
набора.***
Класс `ProductService`.


