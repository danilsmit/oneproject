public class ProductService {
    public static int countByFilter(Shipment shipment, Filter filter){
        int count = 0;
        for (Product i : shipment.getProducts()){
            if (filter.apply(i.getName()))
                count++;
        }
        return count;
    }
}
