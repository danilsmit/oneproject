
import java.util.Arrays;
import java.util.Objects;

public class Shipment  {
    private Product[] products;
    private String description;

    public Shipment(Product[] products, String description){
        if (description == null)
            throw new NullPointerException("");
        for (Product i : products)
            if ( !(i instanceof PackagedPieceGoods || i instanceof PackagedWeightGoods))
                throw new NullPointerException("Только упакованный");
        this.products = products;
        this.description = description;
    }

    public Product[] getProducts() {
        return products;
    }

    public void setProducts(Product[] products) {
        this.products = products;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Shipment shipment = (Shipment) o;
        return Arrays.equals(products, shipment.products) &&
                Objects.equals(description, shipment.description);
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(description);
        result = 31 * result + Arrays.hashCode(products);
        return result;
    }

    @Override
    public String toString() {
        return "Shipment{" +
                "products=" + Arrays.toString(products) +
                ", description='" + description + '\'' +
                '}';
    }

    public double allMuss(){
        double muss = 0;
        for (Product i : products )
            if (i instanceof PackagedWeightGoods)
                muss += ((PackagedWeightGoods) i).getBrutto();
            else if (i instanceof PackagedPieceGoods)
                muss += ((PackagedPieceGoods) i).getMussBrutto();
        return muss;
    }
}
