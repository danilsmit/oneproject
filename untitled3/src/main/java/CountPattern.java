public class CountPattern implements Filter {
    private String pattern;

    public CountPattern(String pattern){
        if (pattern != null)
            this.pattern  = pattern;
        else
            throw new NullPointerException("Пустая строка");
    }

    @Override
    public boolean apply(String string) {
        String[] strings = string.split(" ");
        boolean flag = false;
        for (String i : strings)
            if (i.equals(pattern)) {
                flag = true;
                break;
            }
        return flag;

    }
}
