import java.util.Objects;

public class Packing {
    private String name;
    private double mass;

    public Packing(String name, double mass){
        if (name == null)
            throw new NullPointerException("Null");
        this.name = name;
        if (mass <=0)
            throw new NullPointerException("Масса меньше 0");
        this.mass = mass;
    }

    public Packing(Packing packing){
        this(packing.getName(), packing.getMass());
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getMass() {
        return mass;
    }

    public void setMass(double mass) {
        this.mass = mass;
    }

    @Override
    public String toString() {
        return "Packing{" +
                "name='" + name + '\'' +
                ", mass=" + mass +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Packing)) return false;
        Packing packing = (Packing) o;
        return getMass() == packing.getMass() &&
                getName().equals(packing.getName());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getName(), getMass());
    }
}
