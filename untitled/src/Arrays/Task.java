package Arrays;

import java.util.Scanner;

import static Bases.СheckInput.checkInt;

public class Task {

    public static void main(String[] args)
    {
        Scanner in = new Scanner(System.in);
        int n = 0;
        System.out.println("Введите n: ");
        checkInt(in);
        n = in.nextInt();
        int[] arr = new int [n];
        scanf(arr);
        System.out.println(toString(arr));
        System.out.println("Количество чётных чисел равно" + even(arr));
        System.out.println("Сумма " + sum(arr));
        System.out.println("Количество [a,b] равно " + line(arr));
        if (positive(arr) == true)
            System.out.println("Все числа положительные");
        else System.out.println("Не все числа положительные");
        reverse(arr);
        System.out.println(toString(arr));
    }

    public static void scanf (int[] arr){
        Scanner in = new Scanner(System.in);
        System.out.println("Введите массив: ");
        for(int i = 0; i < arr.length; i++) {
            checkInt(in);
            arr[i] = in.nextInt();
        }
    }


    public static StringBuilder toString(int [] arr){
        StringBuilder sb = new StringBuilder();
        for(int i = 0; i < arr.length; i++)
            sb.append(arr[i]).append(" ");
        sb.append("\n");
        return sb;
    }

    public static int sum(int [] arr){
        int s = 0;
        for(int i = 0; i < arr.length; i++)
            s+=arr[i];
        return s;
    }

    public static int even(int [] arr){
        int count = 0;
        for(int i = 0; i < arr.length; i++){
            if(arr[i] % 2 == 0)
                count++;
        }
        return count;
    }

    public static int line(int [] arr){
        int a = 0,b = 0,count = 0;
        Scanner in = new Scanner(System.in);
        System.out.println("Введите a: ");
        checkInt(in);
        a = in.nextInt();
        System.out.println("Введите b: ");
        checkInt(in);
        b = in.nextInt();
        for(int i = 0; i < arr.length; i++){
            if(arr[i] >= a && arr[i] <= b)
                count++;
        }
        return  count;
    }

    public static boolean positive(int [] arr){

        for(int i = 0; i < arr.length; i++){
            if(arr[i] < 0)
                return false;
        }
        return true;
    }

    public static void reverse(int [] arr){
        int temp;
        for(int i = 0; i < arr.length / 2; i++){
            temp = arr[i];
            arr[i] = arr[arr.length - i - 1];
            arr[arr.length - i - 1] = temp;
        }

    }


}