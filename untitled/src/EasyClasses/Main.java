package EasyClasses;

public class Main {
    public static void main(String[] args) {
        Point3D point1 = new Point3D();
        Point3D point2 = new Point3D(1,0,0);
        Vector3D vector1 = new Vector3D();
        Vector3D vector2 = new Vector3D(1,2,3);
        Vector3D vector3 = new Vector3D(point1, point2);
        Vector3DArray array = new Vector3DArray(3);
        array.setArr(0, vector1);
        array.setArr(1, vector2);
        array.setArr(2, vector3);

        Vector3DProcessor processor = new Vector3DProcessor();

        System.out.println("Первая точка");
        point1.print();
        System.out.println("Вторая точка");
        point2.print();

        /*if(point1.equals(point2)){
            System.out.println("Точки равны");
        }
        else System.out.println("Точки не равны");*/
        //if(point1 == point2){
          //  System.out.println("Точки равны");
       // }
       // else System.out.println("Точки не равны");
        if(point1.equals(point2)){
            System.out.println("Точки равны");
        }
        else System.out.println("Точки не равны");
       /* if(point1.isEqual(point1, point1) == true) {
            System.out.println("Точка1 равна точке1");
        }
        else System.out.println("Точка1 не равна точке1");*/


        System.out.println("Длина 1го вектора " +  vector1.lenght());
        System.out.println("Длина 2го вектора " +  vector2.lenght());
        System.out.println("Длина 3го вектора " +  vector3.lenght());

        if(vector1.checkEqual(vector2) == true)
            System.out.println("Вектора равны");
        else System.out.println("Вектора не равны");

        System.out.println("Сумма " + processor.sum(vector2, vector3).toStringp());
        System.out.println("Разность " + processor.subtraction(vector2, vector3).toStringp());
        System.out.println("Скалярное произведение " + processor.scalarMult(vector2, vector3));
        System.out.println("Векторное произведение " + processor.vectorMult(vector2, vector3).toStringp());
        if(processor.check(vector2, vector3) == true)
            System.out.println("Коллинеарны");
        else  System.out.println("Не коллинеарны");

        Point3D p = new Point3D(4,5,15);
        int k;
        double [] arrayDouble = new double[3];
        arrayDouble[0] = 4.4;
        arrayDouble[1] = 7.1;
        arrayDouble[2] = 15.7;
        System.out.println("Длина массива " + array.length());
        System.out.println(array.toString() + "\n");
        array.replace(vector2);
        System.out.println(array.toString());
        System.out.println("Максимальная длина: " + array.maxVector());
        k = array.search(vector1);
        if (k == -1) {
            System.out.println("Такого же вектора нет");
        } else {
            System.out.println("Индекс вектора: " + k);
        }
        System.out.println("Сумма всех векторов: " + array.summ().toStringp());
        System.out.println("Линейная комбинация векторов " + array.linearComb(arrayDouble).toStringp());
        System.out.println("Массив сдвига точек:");
        for(int i = 0; i < array.move(p).length; i++) {
            System.out.println(array.move(p)[i] + "\n");
        }




    }
}
