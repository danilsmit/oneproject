package EasyClasses;

public class Vector3D {

    private double x,y,z;

    Vector3D(){
        x = 0;
        y = 0;
        z = 0;
    }

    Vector3D(double x, double y, double z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    Vector3D(Point3D a, Point3D b){
        x = a.getX() - b.getX();
        y = a.getY() - b.getY();
        z = a.getZ() - b.getZ();

    }

    public double getX(){
        return x;
    }

    public double getY(){
        return y;
    }

    public double getZ(){
        return z;
    }
    public void setX(double x){
        this.x = x;
    }

    public void setY(double y){
        this.y = y;
    }

    public void setZ(double z){
        this.z = z;
    }


    public double lenght(){
        return Math.sqrt((x*x) + (y*y) + (z*z));
    }

    public boolean checkEqual(Vector3D vector){
        if(Math.abs(x - vector.getX()) < 0.000001 && Math.abs(y - vector.getY()) < 0.000001 && Math.abs(z - vector.getZ()) < 0.000001)
            return  true;
        else return false;
    }

    public String toStringp(){
        StringBuilder sb = new StringBuilder();
        sb.append(x).append(" ").append(y).append(" ").append(z);
        sb.append("\n");
        return sb.toString();
    }

}
