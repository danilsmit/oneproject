package EasyClasses;

public class Vector3DProcessor {


    public  Vector3D sum( Vector3D vector1,  Vector3D vector2) {
        Vector3D result = new Vector3D();
        result.setX(vector1.getX() + vector2.getX());
        result.setY(vector1.getY() + vector2.getY());
        result.setZ(vector1.getZ() + vector2.getZ());
        return result;
    }

    public Vector3D subtraction(Vector3D vector1, Vector3D vector2) {
        Vector3D result = new Vector3D();
        result.setX(vector1.getX() - vector2.getX());
        result.setY(vector1.getY() - vector2.getY());
        result.setZ(vector1.getZ() - vector2.getZ());
        return result;
    }

    public double scalarMult( Vector3D vector1,  Vector3D vector2) {
        double result = vector1.getX() * vector2.getX() + vector1.getY() * vector2.getY() * vector1.getZ() + vector2.getZ();
        return result;
    }

    public Vector3D vectorMult(Vector3D vector1,  Vector3D vector2){
        Vector3D result = new Vector3D();
        result.setX(vector1.getY()*vector2.getZ() - vector1.getZ()*vector2.getY());
        result.setY(vector1.getZ()*vector2.getX() - vector1.getX()*vector2.getZ());
        result.setZ(vector1.getX()*vector2.getY() - vector1.getY()*vector2.getX());
        return result;
    }

    public  boolean check(Vector3D vector1,  Vector3D vector2){
        double k, c,r;
        k = (vector1.getX()/vector2.getX());
        c = (vector1.getY()/vector2.getY());
        r = (vector1.getZ()/vector2.getZ());
        if( Math.abs(k - c) < 0.000001 && Math.abs(k - r) < 0.000001 && Math.abs(r- c) < 0.000001)
            return true;
        else return false;
    }

    public String toString(Vector3D vector){
        StringBuilder sb = new StringBuilder();
        sb.append(vector.getX()).append(" ").append(vector.getY()).append(" ").append(vector.getZ());
        sb.append(" ");
        return sb.toString();
    }


}
