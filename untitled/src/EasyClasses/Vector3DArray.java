package EasyClasses;

import java.util.Scanner;

public class Vector3DArray {
    private Vector3D arr[];

    Vector3DArray(int size){
        arr = new Vector3D[size];
        for (int i = 0; i < size; i++) {
            arr[i] = new Vector3D();
            arr[i].setX(0);
            arr[i].setY(0);
            arr[i].setZ(0);
        }
    }

    public int length(){
        return arr.length;
    }

    public void replace(Vector3D vector){
        Scanner in = new Scanner(System.in);
        boolean flag = true;
        int i = 0;
        while(flag){
            i = in.nextInt();
            if(i >= arr.length)
                System.out.println("Должен быть меньше массива!");
            else flag = false;
        }
        arr[i] = vector;

    }

    public double maxVector(){
        double max = -1;
        double length = 0;
        for(int i = 0; i < arr.length; i++){
            length = Math.sqrt(arr[i].getX() * arr[i].getX() + arr[i].getY()* arr[i].getY() + arr[i].getZ()* arr[i].getZ());
            if(length >= max)
                max = length;
        }
        return  length;
    }

    public int search(Vector3D vector){
        for(int i = 0; i < arr.length; i++){
            if(Math.abs(arr[i].getX() - vector.getX()) < 0.000001 && Math.abs(arr[i].getY() - vector.getY()) < 0.000001 && Math.abs(arr[i].getZ() - vector.getZ()) < 0.000001){
                return i;
            }
        }
        return -1;
    }

    public Vector3D summ(){
        Vector3D sum = new Vector3D();
        for (int i = 0; i < arr.length; i++){
            sum.setX(sum.getX() + arr[i].getX());
            sum.setY(sum.getY() + arr[i].getY());
            sum.setZ(sum.getZ() + arr[i].getZ());
        }
        return sum;
    }

    public Vector3D linearComb(double[] arrDouble){
        Vector3D vector = new Vector3D();
        if(arrDouble.length != arr.length) {
            return vector;
        }
        for(int i = 0; i < arr.length; i++){
            vector.setX(vector.getX() + arr[i].getX() * arrDouble[i]);
            vector.setY(vector.getY() + arr[i].getY() * arrDouble[i]);
            vector.setZ(vector.getZ() + arr[i].getZ() * arrDouble[i]);
        }

        return vector;
    }

    public Point3D[] move(Point3D point){
        Point3D[]arrayPoint = new Point3D[arr.length];
        arrayPoint[0] = new Point3D(1,2,3);
        arrayPoint[1] = new Point3D(1,2,3);
        arrayPoint[2] = new Point3D(1,2,3);
        for(int i = 0; i < arr.length; i++){
            arrayPoint[i].setX(arrayPoint[i].getX() + point.getX());
            arrayPoint[i].setY(arrayPoint[i].getY() + point.getY());
            arrayPoint[i].setZ(arrayPoint[i].getZ() + point.getZ());

        }
        return arrayPoint;
    }

    public void setArr(int i, Vector3D vector) {
        arr[i] = vector;
    }



    public String toString(){
        StringBuilder sb = new StringBuilder();
        for(int i = 0; i < arr.length; i++){
            sb.append(arr[i].getX()).append(" ").append(arr[i].getY()).append(" ").append(arr[i].getZ()).append(" ");
        }
        sb.append("\n");

        return sb.toString();
    }
}