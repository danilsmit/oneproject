package Bases;

import java.util.Scanner;

import static Bases.СheckInput.checkDouble;


public class Four {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        double a, b, c, D;

        System.out.println("Введите a: ");
        checkDouble(in);
        a = in.nextInt();
        System.out.println("Введите b: ");
        checkDouble(in);
        b = in.nextDouble();
        System.out.println("Введите c: ");
        checkDouble(in);
        c = in.nextDouble();
        D = b * b - 4 * a * c;


        if (a == 0 && b != 0) {
            System.out.println("x = " + -c/b);
        } else if (a == 0 && b == 0 && c != 0) {
            System.out.println("Решений нет");
        } else if (a == 0 && b == 0 && c == 0) {
            System.out.println("Бесконечное количество решений");
        } else if (D < 0) {
            System.out.println("Реальных решений нет");
        } else if (D == 0) {
            System.out.println("x1,2 = " + ((-1) * b) / 2 * a);
        } else {
            System.out.println(D);
            System.out.println("x1 = " + (((-1) * b) - Math.sqrt(D)) / (2 * a));
            System.out.println("x2 = " + (((-1) * b) + Math.sqrt(D)) / (2 * a));
        }



    }
}
