package Bases;

import java.util.Scanner;

import static Bases.СheckInput.checkDouble;


public class Five {
    public static void main(String[] args){
        Scanner scan = new Scanner(System.in);
        System.out.println();
        System.out.println("Напишите начало и конец отрезка");
        double a = scan.nextDouble(), b = scan.nextDouble();
        if (b < a){
            double k = a;
            a = b;
            b = k;
        }
        System.out.println("Задайте шаг ");
        double step = scan.nextDouble();
        if (step != 0){
            double res = 0;
            for (double i = a ; Math.abs(b - i) < 1e-9 || i < b  ; i+= step){
                res = Math.sin(i);
                System.out.println("Sin(" + i + ") = " + res );
            }
        }
        else System.out.println("Sin(" + ") = " + Math.sin(a));

    }
}
