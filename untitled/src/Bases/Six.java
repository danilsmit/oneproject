package Bases;

import java.util.Scanner;

import static Bases.СheckInput.checkDouble;

public class Six {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        double a1,a2,b1,b2,c1,c2,x,y;

        System.out.println("Введите a1:");
        checkDouble(in);
        a1 = in.nextDouble();

        System.out.println("Введите b1:");
        checkDouble(in);
        b1 = in.nextDouble();

        System.out.println("Введите c1:");
        checkDouble(in);
        c1 = in.nextDouble();

        System.out.println("Введите a2:");
        checkDouble(in);
        a2 = in.nextDouble();

        System.out.println("Введите b2:");
        checkDouble(in);
        b2 = in.nextDouble();

        System.out.println("Введите c2:");
        checkDouble(in);
        c2 = in.nextDouble();

        if(a1*b2-a2*b1 == 0){
            if (c1*b2-b1*c2 == 0)
                System.out.println("Бесконечно много решений");
            else
                System.out.println("Нет решений");
        }
        else{
            x = (b1*c2-b2*c1)/(a2*b1-a1*b2);
            y = (a2*c1-c2*a1)/(a2*b1-a1*b2);
            System.out.println("x = " + x);
            System.out.println("y = " + y);


        }


    }
}
