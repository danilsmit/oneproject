package Bases;

import java.util.Scanner;

public class СheckInput {

    public static void checkDouble(Scanner in) {
        while (!in.hasNextDouble()) {
            System.out.println("Неверный ввод. Повторите попытку: ");
            in.next();
        }
    }

    public static void checkInt(Scanner in) {
        while (!in.hasNextInt()) {
            System.out.println("Неверный ввод. Повторите попытку: ");
            in.next();
        }
    }
}

