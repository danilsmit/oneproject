import java.util.Arrays;

public class FinanceReport {

    private Payment[] payments;
    private String FIO;
    private int day, month, year;

    public void setPayments(Payment[] payments) {
        this.payments = payments;
    }

    public String getFIO() {
        return FIO;
    }

    public void setFIO(String FIO) {
        this.FIO = FIO;
    }

    public int getDay() {
        return day;
    }

    public void setDay(int day) {
        this.day = day;
    }

    public int getMonth() {
        return month;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public FinanceReport(Payment[] payments, String FIO, int day, int month, int year){
        if (payments != null)
            this.payments = payments;
        else
            throw new NullPointerException("Нет платежей");
        if (FIO != null)
            this.FIO = FIO;
        else
            throw new NullPointerException("");
        if (year <= 0)
            throw new IndexOutOfBoundsException("Год не может быть отрицательным");
        else {
            if (month > 0 && month <= 12) {
                this.month = month;
                if ((month % 2 != 0 && month <= 7) ||
                        (month % 2 == 0 && month >= 8)) {
                    if (day > 0 && day <= 31)
                        this.day = day;
                    else
                        throw new IndexOutOfBoundsException("");
                }else {
                    if (month == 2 &&
                            ((day > 0 && day < 28) || (year % 4 == 0 &&
                                    (year % 100 != 0 && year % 400 == 0))))
                        this.day = day;
                    else if (month != 2 && (day > 0 && day < 30))
                        this.day = day;
                    else
                        throw new IndexOutOfBoundsException("");
                }

            } else
                throw new IndexOutOfBoundsException("Месяц с 1 по 12 чисел");
            this.year = year;
        }
    }

    public FinanceReport (FinanceReport clone){
        this(clone.getPayments().clone(),clone.getFIO(),clone.getDay(),clone.getMonth(),clone.getYear());
    }

    public int getPaymentsLength(){
        return payments.length;
    }

    public Payment[] getPayments(){
        return payments;
    }

    public Payment getElem(int index) {
        if (index < payments.length )
            return payments[index];
        else
            throw new ArrayIndexOutOfBoundsException("");
    }

    public void setElem(int index, Payment elem){
        if (index < payments.length)
            payments[index] = elem;
        else
            throw new ArrayIndexOutOfBoundsException("Вышел за порог массива");
    }

    public String toString() {
        StringBuilder stringBuilder = new StringBuilder("[Автор:" + FIO +
                " дата: "+ day + "." + month + "." + year +
                ", Платежи:[\n") ;
        for (Payment payment : payments)
            stringBuilder.append(String.format("Плательщик: %s, дата: %d.%d.%d сумма: %d \n",
                    payment.getFIO(), payment.getDay(), payment.getMonth(), payment.getYear(),
                    payment.getSum()));
        return stringBuilder.toString() +"]]";
    }

}
