import java.util.Objects;

public class Payment {
    private String FIO;
    private int day, month, year;
    private int sum;

    public Payment(String FIO, int day, int month, int  year, int sum)
    {
        if (FIO != null)
            this.FIO = FIO;
        else
            throw new NullPointerException("У плательщика должно быть имя");

        if (year <= 0)
            throw new IndexOutOfBoundsException("Год не может быть отрицательным");
        else {
            if (month > 0 && month <= 12) {
                this.month = month;
                if ((month % 2 != 0 && month <= 7) ||
                        (month % 2 == 0 && month >= 8)) {
                    if (day > 0 && day <= 31)
                        this.day = day;
                    else
                        throw new IndexOutOfBoundsException("");
                }else {
                    if (month == 2 && Year() && day > 0 && day < 30 )
                        this.day = day;
                    else if (month == 2 && !Year() && day>0 && day<29)
                        this.day = day;
                    else if (month != 2 && (day > 0 && day <= 30))
                        this.day = day;
                    else
                        throw new IndexOutOfBoundsException("Февраль");
                }

            } else
                throw new IndexOutOfBoundsException("Месяц с 1 по 12 чисел");
            this.year = year;
        }
        this.sum = sum;
    }

    public Payment(String FIO, int day, int month, int  year){
        this(FIO, day, month, year, 0);
    }

    public int getDay() {
        return day;
    }

    public int getMonth() {
        return month;
    }

    public int getSum() {
        return sum;
    }

    public int getYear() {
        return year;
    }

    public String getFIO() {
        return FIO;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Payment payment = (Payment) o;
        return day == payment.day &&
                month == payment.month &&
                year == payment.year &&
                sum == payment.sum &&
                Objects.equals(FIO, payment.FIO);
    }

    @Override
    public int hashCode() {
        return Objects.hash(FIO, day, month, year, sum);
    }

    @Override
    public String toString() {
        return "Плательщик(" +
                "ФИО: '" + FIO + '\'' +
                ", день=" + day +
                ", месяц=" + month +
                ", год=" + year +
                ", сумма=" + sum +
                ')';
    }

    private boolean Year(){
        return (year % 4 == 0 && year % 100 != 0) || year % 400 == 0;
    }
}

