import java.util.Scanner;

public class TestFinanceReport {
    public static void main(String[] arg){
        String FIO;
        Scanner scanner = new Scanner(System.in);
        int day, month, year;
        Payment[] payments;
        System.out.print("Фамилия составителя отчета"); FIO = scanner.nextLine();
        System.out.print("день: "); day = scanner.nextInt();
        System.out.print("месяц: "); month = scanner.nextInt();
        System.out.print("год: "); year = scanner.nextInt();
        int n;
        System.out.print("Кол-во платежей"); n = scanner.nextInt();
        payments = new Payment[n];
        String FIO1;
        int day1, month1,year1;
        for (int i = 0; i < n; i ++){
            System.out.print("ФИО: "); FIO1 = new Scanner(System.in).nextLine();
            System.out.print("день: "); day1 = scanner.nextInt();
            System.out.print("месяц: "); month1 = scanner.nextInt();
            System.out.print("год: "); year1 = scanner.nextInt();
            payments[i] = new Payment(FIO1,day1,month1,year1);
        }
        FinanceReport financeReport = new FinanceReport(payments,FIO,day,month,year);
        System.out.println(financeReport);
        int index, sum;
        System.out.print("Индекс плательщика"); index = scanner.nextInt();
        System.out.println(financeReport.getElem(index));
        System.out.println("Напишите новый платеж");
        System.out.print("ФИО: "); FIO1 = new Scanner(System.in).nextLine();
        System.out.print("день: "); day1 = scanner.nextInt();
        System.out.print("месяц: "); month1 = scanner.nextInt();
        System.out.print("год: "); year1 = scanner.nextInt();
        System.out.print("сумма"); sum = scanner.nextInt();
        System.out.print("Индекс замены: "); index = scanner.nextInt();
        FinanceReport financeReport1 = new FinanceReport(financeReport);
        financeReport.setElem(index, new Payment(FIO1,day1,month1,year1,sum));
        System.out.println(financeReport);
        System.out.println("Было"); System.out.println(financeReport1);
        char a;
        System.out.print("На символ который он начинаются"); a = new Scanner(System.in).nextLine().charAt(0);
        System.out.println(FinanceReportProcessor.getPerson(financeReport,a));
        int max;
        System.out.print("Меньше этого числа"); max = scanner.nextInt();
        System.out.println(FinanceReportProcessor.getPerson(financeReport, max));


    }
}
