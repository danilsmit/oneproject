public class FinanceReportProcessor {

    private static Payment[] HER (Payment[] HER,int[] ArrInt) {
        Payment[] payments = null;
        if (HER.length != ArrInt.length)
            throw new ArrayIndexOutOfBoundsException("");
        else {
            int count = 0;
            for (int i : ArrInt)
                if (i != 0 )
                    count++;
            payments = new Payment[count];
            int i = 0, j = 0;
            do {
                if (ArrInt[i] != 0 && ArrInt[i] != -1){
                    payments[j] = HER[ ArrInt[i] ];
                    j++;
                }
                else if (ArrInt[i] == -1){
                    payments[j] = HER[0];
                    j++;
                }
                i++;

            }while (i < ArrInt.length);
        }
        return payments;
    }

    public static  FinanceReport getPerson(FinanceReport financeReport, char a){
        int[] ArrInt = new int[financeReport.getPayments().length];
        for (int i = 0; i < ArrInt.length; i++){
            if (StringProcessor.Inception(financeReport.getPayments()[i].getFIO(), a))
                if (i != 0)
                    ArrInt[i] = i;
                else
                    ArrInt[i] = -1;
        }
        FinanceReport financeReport1 = new FinanceReport(financeReport);
        financeReport1.setPayments(HER(financeReport1.getPayments(), ArrInt));
        return financeReport1;
    }

    private static boolean little(Payment payment, int sum){
        return payment.getSum() < sum;
    }

    public static FinanceReport getPerson(FinanceReport financeReport, int x){
        int[] ArrInt = new int[financeReport.getPayments().length];
        for (int i = 0; i < ArrInt.length; i++){
            if (little(financeReport.getPayments()[i], x))
                if (i != 0)
                    ArrInt[i] = i;
                else
                    ArrInt[i] = -1;
        }
        FinanceReport financeReport1 = new FinanceReport(financeReport);
        financeReport1.setPayments(HER(financeReport1.getPayments(), ArrInt));
        return financeReport1;
    }
}
