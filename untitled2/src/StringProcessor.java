//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//


import java.util.Scanner;

public class StringProcessor {
    public StringProcessor() {
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Scanner scanner1 = new Scanner(System.in);
        System.out.println("На входе строка s и целое число N. Выход — строка, состоящая из N копий строки s,\nзаписанных подряд.");
        System.out.print("Строка s:");
        String s = scanner.nextLine();
        System.out.print("Число N:");
        int n = scanner.nextInt();
        System.out.println(Copy(s, n));
        System.out.println("На входе две строки. Результат — количество вхождений второй строки в первую");
        System.out.print("Строка1:");
        String s1 = scanner1.nextLine();
        System.out.print("Строка2:");
        String s2 = scanner1.nextLine();
        System.out.println(IngoingCounter(s1, s2));
        System.out.println("Постройте по строке новую строку, которая получена из исходной заменой каждого\nсимвола '1' на подстроку ”один”, символа ‘2’ на подстроку “два” и символа ‘3’ на\nподстроку “три”");
        System.out.print("Строка s: ");
        s = (new Scanner(System.in)).nextLine();
        System.out.println(Treason(s));
        System.out.println("В строке типа StringBuilder удалите каждый второй по счету символ. Должна быть\nмодифицирована входная строка, а не порождена новая. ");
        System.out.print("StringBuilder: ");
        StringBuilder stringBuilder = new StringBuilder((new Scanner(System.in)).nextLine());
        System.out.println(Castration(stringBuilder));
    }

    public static boolean Inception(String str, char a) {
        return str.charAt(0) == a;
    }

    public static String Copy(String s, int N) throws StringIndexOutOfBoundsException {
        if (N <= 0) {
            if (N == 0) {
                return "";
            } else {
                throw new StringIndexOutOfBoundsException("Копий не может быть меньше 0");
            }
        } else {
            StringBuilder s2 = new StringBuilder();

            for(int i = 0; i != N; ++i) {
                s2.append(s);
            }

            return s2.toString();
        }
    }

    public static String Treason(String str) {
        StringBuilder stringBuilder = new StringBuilder();
        char[] var2 = str.toCharArray();
        int var3 = var2.length;

        for(int var4 = 0; var4 < var3; ++var4) {
            char i = var2[var4];
            if (i == '1') {
                stringBuilder.append("один");
            } else if (i == '2') {
                stringBuilder.append("два");
            } else if (i == '3') {
                stringBuilder.append("три");
            } else {
                stringBuilder.append(i);
            }
        }

        return stringBuilder.toString();
    }

    public static StringBuilder Castration(StringBuilder stringBuilder) {
        int count = 0;

        for(int i = stringBuilder.length() - 1; i > 0; --i) {
            if (i % 2 != 0) {
                stringBuilder.deleteCharAt(stringBuilder.length() - 1 - count);
            } else {
                ++count;
            }
        }

        return stringBuilder;
    }

    public static int IngoingCounter(String str1, String str2) {
        int count = 0;
        if (str1 != null && str2 != null) {
            char[] data1 = str1.toCharArray();
            char[] data2 = str2.toCharArray();

            for(int i = 0; i < data1.length; ++i) {
                if (data1[i] == data2[0]) {
                    int k = i;

                    for(int j = 0; k < data1.length && j < data2.length && data1[k] == data2[j]; ++k) {
                        if (j == data2.length - 1) {
                            ++count;
                        }

                        ++j;
                    }
                }
            }
        }

        return count;
    }
}
