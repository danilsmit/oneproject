package Keyboard;

import org.junit.Test;
import structural.bridge.Notepad.PersonalNotepad;
import structural.bridge.Page.MegaPage;

import static org.junit.Assert.assertEquals;

public class MegaPageTest {

    @Test
    public void MegaPageTest(){
        PersonalNotepad notepad = new PersonalNotepad();
        notepad.open();
        MegaPage megaPage = new MegaPage(notepad);

        megaPage.setCharacter('t');
        megaPage.setCharacter('e');
        megaPage.setCharacter('s');
        megaPage.setCharacter('t');


        assertEquals("test", notepad.getMemory());
    }

}