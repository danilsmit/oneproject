package behavioral.Memento;

public class Preset {
    private EngineControlUnit engineControlUnit;

    public Preset(EngineControlUnit yourEngineControlUnit) {
        this.engineControlUnit = yourEngineControlUnit;
    }

    public Preset()
    {
        engineControlUnit.setEngineHourse(0);
        engineControlUnit.setIdleSpeed(0);
        engineControlUnit.setLowSpeed(0);
        engineControlUnit.setMaxSpeed(0);
        engineControlUnit.setHP(0);
    }
}
