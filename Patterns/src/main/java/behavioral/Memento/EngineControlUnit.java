package behavioral.Memento;

public class EngineControlUnit {
    private int EngineHourse;
    private int IdleSpeed;
    private int LowSpeed;
    private int MaxSpeed;
    private int Turbo;
    private int HP;

    public int getEngineHourse() {
        return EngineHourse;
    }

    public void setEngineHourse(int engineHourse) {
        this.EngineHourse = engineHourse;
    }

    public int getIdleSpeed() {
        return IdleSpeed;
    }

    public void setIdleSpeed(int idleSpeed) {
        IdleSpeed = idleSpeed;
    }

    public int getLowSpeed() {
        return LowSpeed;
    }

    public void setLowSpeed(int lowSpeed) {
        LowSpeed = lowSpeed;
    }

    public int getMaxSpeed() {
        return MaxSpeed;
    }

    public void setMaxSpeed(int maxSpeed) {
        MaxSpeed = maxSpeed;
    }

    public int getTurbo() {
        return Turbo;
    }

    public void setTurbo(int turbo) {
        Turbo = turbo;
    }

    public int getHP() {
        return HP;
    }

    public void setHP(int HP) {
        this.HP = HP;
    }
}
