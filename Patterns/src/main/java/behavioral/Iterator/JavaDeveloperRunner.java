package behavioral.Iterator;

public class JavaDeveloperRunner {
    public static void main(String[] args) {
        String[] skills={"Java", "Spring", "Hib", "Maven", "Post"};

        JavaDeveloper javaDeveloper= new JavaDeveloper("Alexander", skills);

        Iterator iterator=javaDeveloper.getIterator();

        System.out.println("Developer"+javaDeveloper.getName());
        System.out.println("Skills: ");

        while (iterator.hasNext()){
            System.out.println(iterator.next().toString()+" ");
        }
    }
}
