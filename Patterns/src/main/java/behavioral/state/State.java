package behavioral.state;

public interface State {

    String getState();

}
