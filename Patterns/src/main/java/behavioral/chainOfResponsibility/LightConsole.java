package behavioral.chainOfResponsibility;

public class LightConsole extends Console {
    public LightConsole(int mask) {
        this.mask = mask;
    }

    @Override
    protected void writeMessage(String msg) {
        System.out.println("Light inspection of stage complete: " + msg);
    }
}
