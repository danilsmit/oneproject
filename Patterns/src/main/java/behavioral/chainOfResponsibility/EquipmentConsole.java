package behavioral.chainOfResponsibility;

public class EquipmentConsole extends Console {
    public EquipmentConsole(int mask) {
        this.mask = mask;
    }

    @Override
    protected void writeMessage(String msg) {
        System.out.println("Equipment of stage is complete: " + msg);
    }
}
