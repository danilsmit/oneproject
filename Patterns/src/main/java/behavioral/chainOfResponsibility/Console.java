package behavioral.chainOfResponsibility;

public abstract class Console {
    public static int EQUIPMENT = 7;
    public static int SOUND = 5;
    public static int LIGHT = 3;
    protected int mask;

    protected Console next;

    public Console setNext(Console console) {
        next = console;
        return console;
    }

    public void message(String msg, int priority) {
        if (priority <= mask) {
            writeMessage(msg);
        }
        if (next != null) {
            next.message(msg, priority);
        }
    }

    abstract protected void writeMessage(String msg);
}
