package behavioral.chainOfResponsibility;

public class SoundConsole extends Console {
    public SoundConsole(int mask) {
        this.mask = mask;
    }

    @Override
    protected void writeMessage(String msg) {
        System.out.println("Sound inspection of stage complete: " + msg);
    }
}
