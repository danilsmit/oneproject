package behavioral.command;

public class Database {
    public void insert(){
        System.out.println("Insert");
    }
    public void update(){
        System.out.println("Update");
    }
    public void select(){
        System.out.println("Reading");
    }
    public void delete(){
        System.out.println("Delete");
    }
}
