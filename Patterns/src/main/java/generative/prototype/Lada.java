package generative.prototype;

public class Lada extends CarFactory {
    private double rate;

    public Lada(Lada lada) {
        super(lada);
        this.rate = lada.rate;
    }

    public Lada() {

    }

    public double getRate() {
        return rate;
    }

    public void setRate(double rate) {
        this.rate = rate;
    }

    @Override
    public CarFactory clone() {
        return new Lada(this);
    }
}
