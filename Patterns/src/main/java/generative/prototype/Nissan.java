package generative.prototype;

public class Nissan extends CarFactory {
    private double menacingness;

    public Nissan(Nissan nissan) {
        super(nissan);
        this.menacingness = nissan.menacingness;
    }

    public Nissan() {

    }

    public double getMenacingness() {
        return menacingness;
    }

    public void setMenacingness(double menacingness) {
        this.menacingness = menacingness;
    }

    @Override
    public CarFactory clone() {
        return new Nissan(this);
    }
}
