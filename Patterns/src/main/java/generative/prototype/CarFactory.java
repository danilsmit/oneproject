package generative.prototype;

public abstract class CarFactory {

        private String name;
        private String generate;

        public CarFactory(CarFactory carFactory) {
            this.name = carFactory.name;
            this.generate = carFactory.generate;
        }

        public CarFactory() {

        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getGenerate() {
            return generate;
        }

        public void setGenerate(String generate) {
            this.generate = generate;
        }

        public abstract CarFactory clone();
}
