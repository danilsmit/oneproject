package generative.FactoryMethod;

public class MergeSorting implements ISort {
    public void sort(double [] arr){
        mergeSort(arr,arr.length);
    }

    public static void mergeSort(double[] a, int length) {
        if (length < 2) {
            return;
        }
        int mid = length / 2;
        double[] l = new double[mid];
        double[] r = new double[length - mid];

        for (int i = 0; i < mid; i++) {
            l[i] = a[i];
        }
        for (int i = mid; i < length; i++) {
            r[i - mid] = a[i];
        }
        mergeSort(l, mid);
        mergeSort(r, length - mid);

        merge(a, l, r, mid, length - mid);
    }

    public static void merge(
            double[] a, double[] l, double[] r, int left, int right) {

        int i = 0, j = 0, k = 0;
        while (i < left && j < right) {
            if (l[i] <= r[j]) {
                a[k++] = l[i++];
            }
            else {
                a[k++] = r[j++];
            }
        }
        while (i < left) {
            a[k++] = l[i++];
        }
        while (j < right) {
            a[k++] = r[j++];
        }
    }
}
