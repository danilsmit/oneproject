package generative.FactoryMethod;

public enum SortingType {
    MERGE,
    COUNTING,
    INSERT
}
