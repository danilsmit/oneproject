package generative.FactoryMethod;

public class InsertSorting implements ISort {
    public void sort(double[] array) {
        for (int left = 0; left < array.length; left++) {
            double value = array[left];
            int i = left - 1;
            for (; i >= 0; i--) {
                if (value < array[i]) {
                    array[i + 1] = array[i];
                } else {
                    break;
                }
            }
            array[i + 1] = value;
        }
    }
}
