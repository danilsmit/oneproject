package generative.FactoryMethod;

import java.util.Arrays;

public class CountingSorting implements ISort {
    public void sort(double[] arr)
    {
        int max = (int) Arrays.stream(arr).max().getAsDouble();
        int min = (int) Arrays.stream(arr).min().getAsDouble();
        int range = max - min + 1;
        double[] count = new double[range];
        double[] output = new double[arr.length];
        for (double v : arr) {
            count[(int) (v - min)]++;
        }

        for (int i = 1; i < count.length; i++) {
            count[i] += count[i - 1];
        }

        for (int i = arr.length - 1; i >= 0; i--) {
            output[(int) (count[(int) (arr[i] - min)] - 1)] = arr[i];
            count[(int) (arr[i] - min)]--;
        }

        System.arraycopy(output, 0, arr, 0, arr.length);
    }
}
