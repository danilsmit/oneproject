package generative.AbstractFactory;

public class AppleTV implements TV {
    @Override
    public void create() {
        System.out.println("Apple TV was created!");
    }
}
