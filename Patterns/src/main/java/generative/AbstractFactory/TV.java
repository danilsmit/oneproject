package generative.AbstractFactory;

public interface TV {
    void create();
}
