package generative.AbstractFactory;

public class Apple implements Gadget {
    public TV createTV()
    {
        return new AppleTV();
    }
    public Phone createPhone(){
        return new ApplePhone();
    }
}
