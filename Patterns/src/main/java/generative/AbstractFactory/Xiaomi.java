package generative.AbstractFactory;

public class Xiaomi implements Gadget {
    public TV createTV()
    {
        return new XiaomiTV();
    }
    public Phone createPhone(){
        return new XiaomiPhone();
    }
}
