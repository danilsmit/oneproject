package generative.AbstractFactory;

public interface Gadget {
    TV createTV();
    Phone createPhone();
}
