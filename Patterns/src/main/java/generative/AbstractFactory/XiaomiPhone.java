package generative.AbstractFactory;

public class XiaomiPhone implements Phone {
    @Override
    public void create() {
        System.out.println("Xiaomi phone was created!");
    }
}
