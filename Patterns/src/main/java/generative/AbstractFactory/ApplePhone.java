package generative.AbstractFactory;

public class ApplePhone implements Phone {
    @Override
    public void create() {
        System.out.println("Iphone was created!");
    }
}
