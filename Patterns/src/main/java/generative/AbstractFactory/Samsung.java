package generative.AbstractFactory;

public class Samsung implements Gadget {
    public TV createTV()
    {
        return new SamsungTV();
    }
    public Phone createPhone(){
        return new SamsungPhone();
    }
}
