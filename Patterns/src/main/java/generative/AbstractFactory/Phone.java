package generative.AbstractFactory;

public interface Phone {
    void create();
}
