package generative.AbstractFactory;

public class SamsungTV implements TV {
    @Override
    public void create() {
        System.out.println("Samsung TV was created!");
    }
}
