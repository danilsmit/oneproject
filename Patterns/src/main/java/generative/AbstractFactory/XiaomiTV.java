package generative.AbstractFactory;

public class XiaomiTV implements TV {
    @Override
    public void create() {
        System.out.println("Xiaomi TV was created!");
    }
}
