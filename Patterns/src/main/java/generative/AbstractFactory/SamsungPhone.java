package generative.AbstractFactory;

public class SamsungPhone implements Phone {
    @Override
    public void create() {
        System.out.println("Samsung phone was created!");
    }
}
