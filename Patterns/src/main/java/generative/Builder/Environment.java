package generative.Builder;

import java.util.Objects;

public class Environment {
    private String Monitors;
    private String HeadPhones;
    private int Mouse;
    private String Keyboards;
    private int Instruments;
    private int Book;

    public void setMonitors(String monitors) {
        this.Monitors = monitors;
    }

    public void setHeadPhones(String headPhones) {
        this.HeadPhones = headPhones;
    }

    public void setMouse(int mouse) {
        this.Mouse = mouse;
    }

    public void setKeyboards(String keyboards) {
        this.Keyboards = keyboards;
    }

    public void setInstruments(int instruments) {
        this.Instruments = instruments;
    }

    public void setBook(int book) {
        this.Book = book;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Environment environment = (Environment) o;
        return Mouse == environment.Mouse &&
                Instruments == environment.Instruments &&
                Book == environment.Book &&
                Objects.equals(Monitors, environment.Monitors) &&
                Objects.equals(HeadPhones, environment.HeadPhones) &&
                Objects.equals(Keyboards, environment.Keyboards);
    }

    @Override
    public int hashCode() {
        return Objects.hash(Monitors, HeadPhones, Mouse, Keyboards, Instruments, Book);
    }
}
