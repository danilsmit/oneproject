package generative.Builder;

public class SeniorDevelopment extends Builder{
    public void bringMonitors() {
        environment.setMonitors("Xiaomi 34' Gaming Display");
    }

    public void bringHeadphones() {
        environment.setHeadPhones("senheizer");
    }

    public void bringMouse() {
        environment.setMouse(4);
    }

    public void bringKeyboards() {
        environment.setKeyboards("Logiteck");
    }

    public void bringInstruments() {
        environment.setInstruments(15);
    }

    public void bringBook() {
        environment.setBook(6);
    }
}
