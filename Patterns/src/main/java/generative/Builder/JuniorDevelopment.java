package generative.Builder;

public class JuniorDevelopment extends Builder {
    public void bringMonitors() {
        environment.setMonitors("Samsung C24");
    }

    public void bringHeadphones() {
        environment.setHeadPhones("Sven");
    }

    public void bringMouse() {
        environment.setMouse(1);
    }

    public void bringKeyboards() {
        environment.setKeyboards(null);
    }

    public void bringInstruments() {
        environment.setInstruments(6);
    }

    public void bringBook() {
        environment.setBook(2);
    }
}
