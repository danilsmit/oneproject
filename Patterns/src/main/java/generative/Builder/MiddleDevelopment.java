package generative.Builder;

public class MiddleDevelopment extends Builder {
    public void bringMonitors() {
        environment.setMonitors("Samsung Odyssey g7");
    }

    public void bringHeadphones() {
        environment.setHeadPhones("Senheizer");
    }

    public void bringMouse() {
        environment.setMouse(2);
    }

    public void bringKeyboards() {
        environment.setKeyboards(null);
    }

    public void bringInstruments() {
        environment.setInstruments(15);
    }

    public void bringBook() {
        environment.setBook(6);
    }
}
