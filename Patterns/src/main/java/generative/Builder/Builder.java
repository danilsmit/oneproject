package generative.Builder;

public abstract class Builder {
    protected Environment environment;

    public Environment getDevelopment()
    {
        return environment;
    }

    public void createDevelopment()
    {
        environment = new Environment();
    }
    public abstract void bringMonitors();
    public abstract void bringHeadphones();
    public abstract void bringMouse();
    public abstract void bringKeyboards();
    public abstract void bringInstruments();
    public abstract void bringBook();

}
