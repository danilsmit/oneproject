package generative.Builder;

public class Director {
    private Builder builder;

    public void setBuilder(Builder builder)
    {
        this.builder = builder;
    }
    public Environment getStudio()
    {
        return builder.getDevelopment();
    }
    public void buildStudio()
    {
        builder.createDevelopment();
        builder.bringMonitors();
        builder.bringHeadphones();
        builder.bringMouse();
        builder.bringKeyboards();
        builder.bringInstruments();
        builder.bringBook();
    }
}
