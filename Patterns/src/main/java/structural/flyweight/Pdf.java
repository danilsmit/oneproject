package structural.flyweight;

public class Pdf implements Format {
    private boolean busy;

    public Pdf(){
        this.busy = false;
    }

    @Override
    public void doWork() {
        this.busy = true;
    }

    @Override
    public boolean isWork() {
        return this.busy;
    }
}
