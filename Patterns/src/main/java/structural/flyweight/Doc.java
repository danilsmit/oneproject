package structural.flyweight;

public class Doc implements Format {
    private boolean busy;

    public Doc(){
        this.busy = false;
    }

    @Override
    public void doWork() {
        this.busy = true;
    }

    @Override
    public boolean isWork() {
        return this.busy;
    }
}
