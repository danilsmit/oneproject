package structural.flyweight;

import java.util.HashMap;
import java.util.Map;

public class Container {
    private static Map<String, Format> container = new HashMap<>();

    public Container(){}

    public static Format getTool(String selectedTool){
        Format format = container.get(selectedTool);

        if(format == null){
            switch (selectedTool){
                case "Pdf":
                    format = new Pdf();
                    break;
                case "Doc":
                    format = new Doc();
            }
            container.put(selectedTool, format);
        }
        return format;

    }

}
