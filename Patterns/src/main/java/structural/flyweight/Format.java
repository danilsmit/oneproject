package structural.flyweight;

public interface Format {

    void doWork();

    boolean isWork();

}
