package structural.bridge.Page;

public interface Page {


    void setCharacter(char character);

    void deleteCharacter();

    String signature(String signatureTranslated);

}
