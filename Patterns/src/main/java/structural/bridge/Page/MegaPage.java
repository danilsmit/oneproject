package structural.bridge.Page;

import structural.bridge.Notepad.Notepad;

public class MegaPage extends MiniPage {

    public MegaPage(Notepad notepad){
        super.notepad = notepad;
    }

    public void enable(){
        notepad.open();
    }

    public void finishWork(){
        notepad.end();
    }

    public void sleepMode(){
        notepad.close();
    }
}
