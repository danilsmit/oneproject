package structural.bridge.Page;

import structural.bridge.Notepad.Notepad;

public class MiniPage implements Page {
    protected Notepad notepad;

    public MiniPage(){};

    public MiniPage(Notepad notepad){
        this.notepad = notepad;
    }

    @Override
    public void setCharacter(char character) {
        notepad.setCharacter(character);
    }

    @Override
    public void deleteCharacter() {
        notepad.deleteCharacter();
    }

    @Override
    public String signature(String signatureTranslated) {
        return notepad.signature(signatureTranslated);
    }
}
