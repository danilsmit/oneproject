package structural.bridge.Notepad;

public interface Notepad {

    void open();

    void end();

    void close();

    void setCharacter(char character);

    String getMemory();

    void deleteCharacter();

    String signature(String hotKeyTranslated);

}
