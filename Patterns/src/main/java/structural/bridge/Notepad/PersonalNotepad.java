package structural.bridge.Notepad;

public class PersonalNotepad implements Notepad {
    private boolean on = false;
    private char lastCharacter;
    private StringBuilder memory;

    @Override
    public void open() {
        memory = new StringBuilder();
        this.on = true;
    }

    @Override
    public void end() {
        this.memory = null;
        this.on = false;
    }

    @Override
    public void close() {
        this.on = false;
    }

    @Override
    public void setCharacter(char lastCharacter) {
        this.lastCharacter = lastCharacter;
        this.memory.append(lastCharacter);
    }

    @Override
    public String getMemory() {
        return this.memory.toString();
    }

    @Override
    public void deleteCharacter() {
        this.memory.deleteCharAt(memory.length()-1);
    }

    @Override
    public String signature(String signatureTranslated) {
            return "signature";
    }
}
