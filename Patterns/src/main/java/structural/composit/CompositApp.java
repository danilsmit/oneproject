package structural.composit;

import java.util.ArrayList;
import java.util.List;

interface Shape{
    void draw();
}
class Square implements Shape{
    public void draw(){
        System.out.println("Квадрат");
    }
}
class Triangle implements Shape{
    public void draw(){
        System.out.println("Труегольник");
    }
}
class Circle implements Shape{
    public void draw(){
        System.out.println("Круг");
    }
}
class Composite implements Shape{
    private List<Shape> components= new ArrayList<>();
    public void addComponent(Shape component){
        components.add(component);
    }
    public void draw() {
        for(Shape component: components){
            component.draw();
        }
    }
}
