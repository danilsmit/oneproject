package structural.Decorator;

public interface Car {
    public int getSpeed();
    public int getBaggageWeight();
}
