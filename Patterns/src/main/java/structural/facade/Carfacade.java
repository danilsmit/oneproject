package structural.facade;

import structural.facade.parts.Door;
import structural.facade.parts.Wheel;
import structural.facade.parts.Zav;

public class Carfacade {
    private Door door= new Door();
    private Zav zav=new Zav();
    private Wheel wheel= new Wheel();

    public void go(){
        door.open();
        zav.fire();
        wheel.turn();
    }
}
