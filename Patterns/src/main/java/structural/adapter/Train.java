package structural.adapter;

import java.util.Objects;

public class Train implements RailTrack {
    private String nameOfTrain;
    private int yearOfBuild;

    public Train(String nameOfTrain, int yearOfBuild) {
        this.nameOfTrain = nameOfTrain;
        this.yearOfBuild = yearOfBuild;
    }

    public String getNameOfTrain() {
        return nameOfTrain;
    }

    public void setNameOfTrain(String nameOfTrain) {
        this.nameOfTrain = nameOfTrain;
    }

    public int getYearOfBuild() {
        return yearOfBuild;
    }

    public void setYearOfBuild(int yearOfBuild) {
        this.yearOfBuild = yearOfBuild;
    }

    @Override
    public void canRail() {
        System.out.println("Этот транспорт может ехать по рельсам!");
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Train train = (Train) o;
        return yearOfBuild == train.yearOfBuild &&
                Objects.equals(nameOfTrain, train.nameOfTrain);
    }

    @Override
    public int hashCode() {
        return Objects.hash(nameOfTrain, yearOfBuild);
    }


}
