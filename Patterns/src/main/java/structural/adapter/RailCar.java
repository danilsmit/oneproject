package structural.adapter;

public class RailCar extends Car implements RailTrack {
    public RailCar(String brand, int yearOfBuild) {
        super(brand, yearOfBuild);
    }

    @Override
    public void canRail() {
        System.out.println("Этот транспорт может ехать по рельсам!");
    }
}
