package structural.adapter;

public interface RailTrack {
    void canRail();
}
