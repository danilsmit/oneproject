package structural.adapter;

import java.util.Objects;

public class Car {
    private String brand;
    private int yearOfBuild;

    public Car(String brand,int yearOfBuild)
    {
        this.brand=brand;
        this.yearOfBuild=yearOfBuild;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public int getYearOfBuild() {
        return yearOfBuild;
    }

    public void setYearOfBuild(int yearOfBuild) {
        this.yearOfBuild = yearOfBuild;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Car car = (Car) o;
        return yearOfBuild == car.yearOfBuild &&
                Objects.equals(brand, car.brand);
    }

    @Override
    public int hashCode() {
        return Objects.hash(brand, yearOfBuild);
    }
}
