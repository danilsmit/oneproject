package ru.omsu.imit.house_data.house_service;

import com.fasterxml.jackson.core.JsonProcessingException;
import org.junit.jupiter.api.Test;
import ru.omsu.imit.house_data.Flat;
import ru.omsu.imit.house_data.House;
import ru.omsu.imit.house_data.Person;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;

import static org.junit.jupiter.api.Assertions.*;

class HouseJacksonSerializerTest {

    @Test
    void houseJacksonSerializeAndDeserialize() throws JsonProcessingException {
        House house = new House("13","Kazakhstan street", new Person(
                "Akakiy", "Nurmagombetov", "Samovich", new Date()),
                new ArrayList<>(Arrays.asList(new Flat(1,80, new ArrayList<>(
                        Arrays.asList(new Person("Akakiy", "Nurmagombetov", "Samovich", new Date())))))));
        String string = HouseJacksonSerializer.houseJacksonSerialize(house);
        assertEquals(house, HouseJacksonSerializer.houseJacksonDeserialize(string));
    }

}