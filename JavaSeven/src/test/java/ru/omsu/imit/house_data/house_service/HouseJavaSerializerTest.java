package ru.omsu.imit.house_data.house_service;

import org.junit.jupiter.api.Test;
import ru.omsu.imit.house_data.Flat;
import ru.omsu.imit.house_data.House;
import ru.omsu.imit.house_data.Person;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;

import static org.junit.jupiter.api.Assertions.*;

class HouseJavaSerializerTest {

    @Test
    void houseSerializeAndDeserialize() throws IOException, ClassNotFoundException {
        House house = new House("13","Kazakhstan street", new Person(
                "Akakiy", "Nurmagombetov", "Samovich", new Date()),
                new ArrayList<>(Arrays.asList(new Flat(1,80, new ArrayList<>(
                        Arrays.asList(new Person("Akakiy", "Nurmagombetov", "Samovich", new Date())))))));
        ObjectOutput objectOutput = new ObjectOutputStream(new BufferedOutputStream(
                new FileOutputStream("houseSerializedWithJava.txt")));
        HouseJavaSerializer.houseSerialize(house, objectOutput);
        objectOutput.close();
        ObjectInput objectInput = new ObjectInputStream(new BufferedInputStream(
                new FileInputStream("houseSerializedWithJava.txt")));
        assertEquals(house, HouseJavaSerializer.houseDeserialize(objectInput));
    }


}