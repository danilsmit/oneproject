package ru.omsu.imit;

import org.junit.jupiter.api.Test;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class InputOutputTest {

    @Test
    void writeAndReadBinaryStream() throws IOException {
        FileInputStream fileInputStream = new FileInputStream("data.txt");
        FileOutputStream fileOutputStream = new FileOutputStream("data.txt");
        int[] array = new int[4];
        array[0] = 1000;
        array[1] = 1001;
        array[2] = -1;
        InputOutput.writeToBinaryStream(fileOutputStream, array);
        array[0] = 0;
        array[1] = 0;
        array[2] = 0;
        InputOutput.readFromBinaryStream(fileInputStream, array);
        assertEquals(1000, array[0]);
        assertEquals(1001, array[1]);
        assertEquals(-1, array[2]);
    }

    /* Филиппов А.В. 23.11.2020 Комментарий не удалять.
    Не работает.
     Бесполезный тест, который проверяет только на отсутствие исключений.
     Переписать в стиле предыдущего теста.
    */
    @Test
    void writeAndReadCharStream() throws IOException {
        int[] buffer1 = {0, 25, -38, 6, -1, -100, 90};
        int[] buffer2 = new int[buffer1.length];

        FileWriter fileWriter = new FileWriter("dataChar.txt");
        InputOutput.writeToCharStream(fileWriter, buffer1);
        fileWriter.close();

        FileReader fileReader = new FileReader("dataChar.txt");
        InputOutput.readFromCharStream(fileReader, buffer2);
        fileWriter.close();

        assertArrayEquals(buffer2, buffer1);
    }

    @Test
    void readFromPosition() throws IOException {
        Integer[] arr = InputOutput.readFromPosition("dataChar.txt", 5);
        assertArrayEquals(new Integer[]{-38, 6, -1, -100, 90}, arr);
    }

    @Test
    void filesWithExtension() {
        List<File> fileList = InputOutput.filesWithExtension(
                System.getProperty("user.dir"), ".xml");

        assertEquals(1, fileList.size());
        String nameFile = fileList.get(0).getName();

        assertEquals("pom.xml", nameFile);
    }
}