package ru.omsu.imit;

import java.io.*;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

public class InputOutput {
    //Записать в байтовый поток массив целых чисел
    public static void writeToBinaryStream(OutputStream in,int[] arr) throws IOException {
        try (DataOutputStream dataOutputStream = new DataOutputStream(in)) {
            for (int i = 0; i < arr.length; i++)
                dataOutputStream.writeInt(arr[i]);
        }

    }

     public static void readFromBinaryStream(InputStream stream, int[] arr) throws IOException {

        try (DataInputStream dataInputStream=new DataInputStream(stream)){
            int buf;
            for (int i = 0; i < arr.length; i++) {
                buf = dataInputStream.readInt();
                arr[i] = buf;
            }
        }}

    //Записать в символьный поток массив целых чисел
    public static void writeToCharStream(Writer writer, int[] array) throws IOException {
        for (int j : array) {
            writer.write(String.valueOf(j));
            writer.write(' ');
        }
    }

    /* Филиппов А.В. 28.11.2020 Комментарий не удалять.
     сканер умеет nextInt()
    */
    //Считать из символьного потока массив целых чисел
    public static void readFromCharStream(Reader reader, int[] arr) {
        Scanner scanner = new Scanner(reader);
        for (int i = 0; i < arr.length; i++) {
            if (!scanner.hasNext())
                return;
            arr[i] = Integer.parseInt(scanner.next());
        }
    }

    /* Филиппов А.В. 23.11.2020 Комментарий не удалять.
     Не работает. Задание звучит так

    3. Используя класс RandomAccessFile, прочитайте массив целых чисел, начиная с заданной позиции.

    Т.е. на вход кроме файла, позиции передаем еще массив целых, в который читаем данные.
    */

    /* Филиппов А.В. 28.11.2020 Комментарий не удалять.
     Не работает. Читаем массив целых чисел, а не строку.
    */
    //Считать массив символов с определённой позиции
    public static Integer[] readFromPosition(String path, long pos) throws IOException {
        List<Integer> out= new LinkedList<>();
        try (RandomAccessFile file = new RandomAccessFile(path, "r")) {
            file.seek(pos);
            StringBuilder num = new StringBuilder();
            int ch = file.read();
            while (ch != -1) {
                if(ch==' '){
                if(num.length()!=0){
                    out.add(Integer.parseInt(num.toString()));
                    num.setLength(0);
                }
            }else num.append((char) ch);
                ch =file.read();
        }
        }
        return out.toArray(new Integer[0]);
    }

    //Используя класс File, получите список всех файлов с заданным расширением в заданном каталоге
    public static List<File> filesWithExtension(String catalog, String extension){
        File currentCatalog = new File(catalog);
        if(!currentCatalog.isDirectory()){
            throw new IllegalArgumentException("It is not directory");
        }
        File[] filesList = currentCatalog.listFiles();
        if(filesList == null){
            return new ArrayList<>();
        }
        List<File> result = new ArrayList<>(filesList.length);
        for(File elem: filesList){
            if(elem.isFile() && elem.getName().endsWith(extension)){
                result.add(elem);
            }
        }
        return result;
    }


}
