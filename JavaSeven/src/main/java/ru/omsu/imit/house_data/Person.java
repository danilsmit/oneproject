package ru.omsu.imit.house_data;

import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

public class Person implements Serializable{
    private String name;
    private String surname;
    private String middleName;
    private Date dateOfBirth;

    public Person() {
        this.name = "";
        this.surname = "";
        this.middleName = "";
        this.dateOfBirth = new Date();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Person person = (Person) o;
        return name.equals(person.name) &&
                surname.equals(person.surname) &&
                middleName.equals(person.middleName) &&
                dateOfBirth.equals(person.dateOfBirth);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, surname, middleName, dateOfBirth);
    }

    public Person(String name, String surname, String middleName, Date dateOfBirth) {
        this.name = name.trim();
        this.surname = surname.trim();
        this.middleName = middleName.trim();
        this.dateOfBirth = dateOfBirth;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public Date getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(Date dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }
}
