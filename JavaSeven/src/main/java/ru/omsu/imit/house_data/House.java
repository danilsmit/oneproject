package ru.omsu.imit.house_data;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;

public class House implements Serializable {
    private String cadastralNum;
    private String address;
    private Person housewife;
    private List<Flat> apartments;

    public House(){
        this.cadastralNum = "";
        this.address = "";
        this.housewife = null;
        this.apartments = null;
    }

    public House(String cadastralNum, String address, Person housewife, List<Flat> apartments) {
        this.cadastralNum = cadastralNum;
        this.address = address;
        this.housewife = housewife;
        this.apartments = apartments;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        House house = (House) o;
        return cadastralNum.equals(house.cadastralNum) &&
                address.equals(house.address) &&
                Objects.equals(housewife, house.housewife) &&
                apartments.equals(house.apartments);
    }

    @Override
    public int hashCode() {
        return Objects.hash(cadastralNum, address, housewife, apartments);
    }

    public String getCadastralNum() {
        return cadastralNum;
    }

    public void setCadastralNum(String cadastralNum) {
        this.cadastralNum = cadastralNum;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Person getHousewife() {
        return housewife;
    }

    public void setHousewife(Person housewife) {
        this.housewife = housewife;
    }

    public List<Flat> getApartments() {
        return apartments;
    }

    public void setApartments(List<Flat> apartments) {
        this.apartments = apartments;
    }
}
