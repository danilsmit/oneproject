package ru.omsu.imit.house_data.house_service;

import ru.omsu.imit.house_data.House;

import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

public class HouseJavaSerializer {
    public static void houseSerialize(House house, ObjectOutput output) throws IOException {
        output.writeObject(house);
    }

    public static House houseDeserialize(ObjectInput input) throws IOException, ClassNotFoundException, ClassCastException {
        return (House) input.readObject();
    }

}
