package ru.omsu.imit.house_data.house_service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import ru.omsu.imit.house_data.House;

public class HouseJacksonSerializer {
    public static String houseJacksonSerialize(House house) throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        return mapper.writeValueAsString(house);
    }

    public static House houseJacksonDeserialize(String stringWithObject) throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        return mapper.readValue(stringWithObject, House.class);
    }

}
